﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace HomeWorkPSQLConsole
{
    class Program
    {
        public static readonly ConsoleColor TableNameColor = ConsoleColor.DarkGray;
        public static readonly ConsoleColor ColumnNameColor = ConsoleColor.Gray;
        public static IProxy Proxy;
        static void Main(string[] args)
        {
            try
            {
                var configuration = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json", true, true)
                    .Build(); 

                var cs = configuration.GetSection("DbInitialSettings").GetValue<string>("ConnectionString");
                Proxy = ProxyGenerator.GetProxy(cs);

                ColorConsole.WriteLine(Proxy.TestConnection(), ConsoleColor.DarkYellow);
               

                var tables = GetTables();

                Console.WriteLine("Need init db? \n\ty - yes");
                var initDb = Console.ReadKey();
                if (initDb.Key == ConsoleKey.Y)
                    foreach (var table in tables)
                        table.Init();

                var ind = -1;
                WorkWithDb(tables, ind);
            }
            catch (Exception e)
            {
                ColorConsole.WriteLine(e.Message, ConsoleColor.Red );
            }
        }

     

        private static List<ITable> GetTables()
        {
            var tables = new List<ITable>
            {
                new StudentsTable("Students", Proxy),
                new CourseTable("Courses", Proxy),
                new CourseSchemeTable("Course scheme", Proxy)
            };
            return tables;
        }

        private static void WorkWithDb(List<ITable> tables, int ind)
        {
            do
            {
                Console.WriteLine("Select action:");
                Console.WriteLine($"\ta - add new");
                Console.WriteLine($"\tt - show selected table");
                Console.WriteLine($"\ts - show all");
                Console.WriteLine($"\tq - quit program");

                var actionNumber = Console.ReadKey();
                if (actionNumber.Key == ConsoleKey.Q)
                    break;
                if (actionNumber.Key != ConsoleKey.S)
                {
                    Console.WriteLine("Select table for insert data");
                    Console.WriteLine($"\tq - quit program");
                    for (var i = 0; i < tables.Count; i++)
                    {
                        var table = tables[i];
                        Console.WriteLine($"\t{i} - for {table.Name}");
                    }

                    var tabNumber = Console.ReadKey();

                    if (tabNumber.Key == ConsoleKey.Q)
                        break;

                    if (int.TryParse(tabNumber.KeyChar.ToString(), out ind))
                    {
                        if (tables.Count <= ind)
                            continue;
                    }
                    else
                    {
                        continue;
                    }
                }

                switch (actionNumber.Key)
                {
                    case ConsoleKey.A:
                        tables[ind].Add(GetData);
                        ColorConsole.WriteLine("Added", ConsoleColor.Green);
                        break;
                    case ConsoleKey.S:
                        foreach (var table in tables)
                        {
                            ColorConsole.WriteLine($"Table {table.Name}", TableNameColor);
                            table.ReadAll(ShowData);
                        }
                        break;
                    case ConsoleKey.T:
                        ColorConsole.WriteLine($"Table {tables[ind].Name}", TableNameColor);
                        tables[ind].ReadAll(ShowData);
                        break;
                    default:
                        continue;
                }

                Console.WriteLine();
                ColorConsole.WriteLine("-----------------------", ConsoleColor.Yellow);
                Console.WriteLine();
            } while (true);
        }

        private static string GetData(string s)
        {
            Console.WriteLine($"{s}");
            return Console.ReadLine();
        }
        private static void ShowData(string s)
        {
            ColorConsole.WriteLine(s, ColumnNameColor);
        }
        
    }
}

