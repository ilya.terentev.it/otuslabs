﻿using System;

namespace HomeWorkPSQLConsole
{
    public interface ITable
    {
        string Name { get; }
        void Init();
        void ReadAll(Action<string> showData);
        void Add(Func<string, string> getData);
    }
}