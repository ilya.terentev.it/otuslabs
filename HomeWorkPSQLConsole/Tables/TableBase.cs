﻿using System;
using System.Collections.Generic;

namespace HomeWorkPSQLConsole
{
    public abstract class TableBase: ITable
    {
        protected readonly IProxy _proxy;
        protected TableBase(string name, IProxy proxy)
        {
            Name = name;
            _proxy = proxy;
        }
        protected void ReadTable(string tableName, Action headerFunc, Action<List<string>> readFunc)
        {
            var sql = $"SELECT * FROM {tableName}";
            var data = _proxy.GetData(sql);
            headerFunc();
            readFunc(data);
        }

        public string Name { get; }
        public abstract void Init();

        public abstract void ReadAll(Action<string> showData);

        public abstract void Add(Func<string, string> getData);
    }
}