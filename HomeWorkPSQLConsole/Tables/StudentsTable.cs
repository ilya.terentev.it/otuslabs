﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWorkPSQLConsole
{
    public class StudentsTable : TableBase
    {
        public StudentsTable(string name, IProxy proxy) : base(name, proxy)
        {

        }

        public override void Init()
        {
            var sb = new StringBuilder();

            sb.Append("CREATE SEQUENCE public.student_id_seq    ");
            sb.Append("INCREMENT 1                              ");
            sb.Append("START 1                                  ");
            sb.Append("MINVALUE 1;                               ");
            sb.Append("ALTER SEQUENCE public.student_id_seq     ");
            sb.Append(@"    OWNER TO ""OTUSUser"";              ");
            sb.Append("CREATE TABLE IF NOT EXISTS public.students");
            sb.Append("(");
            sb.Append("    id bigint NOT NULL DEFAULT nextval('student_id_seq'::regclass),");
            sb.Append(@"    name character varying COLLATE pg_catalog.""default"" NOT NULL,");
            sb.Append(@"    mid_name character varying COLLATE pg_catalog.""default"",");
            sb.Append(@"    surname character varying COLLATE pg_catalog.""default"" NOT NULL,");
            sb.Append(@"    CONSTRAINT ""Students_pkey"" PRIMARY KEY (id)");
            sb.Append(")");
            sb.Append("TABLESPACE pg_default;");
            sb.Append("ALTER TABLE public.students");
            sb.Append(@"    OWNER to ""OTUSUser"";");
            sb.Append("INSERT INTO public.students(name, mid_name, surname) VALUES ('Bob', 'Ivanovich', 'Black');");
            sb.Append("INSERT INTO public.students(name, mid_name, surname) VALUES ('Clark', 'Ivanovich', 'White');");
            sb.Append("INSERT INTO public.students(name, mid_name, surname) VALUES ('Addy', 'Ivanovich', 'Red');");
            sb.Append("INSERT INTO public.students(name, mid_name, surname) VALUES ('Mindy', 'Alexandrovna', 'Yellow');");
            sb.Append("INSERT INTO public.students(name, mid_name, surname) VALUES ('Bobby', 'Ivanovich', 'Green');");
            sb.Append("INSERT INTO public.students(name, mid_name, surname) VALUES ('BobbyJ', 'Ivanovich', 'Smit');");
            
            var commandText = sb.ToString();
            _proxy.SendCommand(commandText);
        }
        

        public override void ReadAll(Action<string> showData)
        {
            var tableName = "students";
            ReadTable(tableName,
                () =>
                {
                    showData($"id\tname\tmid_name\tsurname "); },
                data =>
                {
                    showData(
                        $"{string.Join(Environment.NewLine, data)}");
                });
        }
        


        public override void Add(Func<string, string> GetData)
        {
            var paramList = new List<(string name, object value)>
            {
                new("NAME", GetData("Enter name")),
                new("MID_NAME", GetData("Enter mid name")),
                new("SURNAME", GetData("Enter surname"))
            };

            var commandText = "INSERT INTO public.students(name, mid_name, surname)";
            _proxy.SendCommand(commandText, paramList);
        }
    }
}
