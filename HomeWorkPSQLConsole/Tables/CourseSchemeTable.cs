﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;
using NpgsqlTypes;

namespace HomeWorkPSQLConsole
{
    public class CourseSchemeTable : TableBase
    {
        public CourseSchemeTable(string name, IProxy proxy) : base(name, proxy)
        {
        }

        public override void Init()
        {
            var sb = new StringBuilder();
            sb.Append("CREATE TABLE IF NOT EXISTS public.cours_scheme                     ");
            sb.Append("(                                                                  ");
            sb.Append("guid uuid NOT NULL DEFAULT gen_random_uuid(),                      ");
            sb.Append("student_id bigint NOT NULL,                                        ");
            sb.Append("    course_id bigint NOT NULL,                                     ");
            sb.Append("CONSTRAINT cours_scheme_pkey PRIMARY KEY (guid),                   ");
            sb.Append("CONSTRAINT course_id_key FOREIGN KEY (course_id)                   ");
            sb.Append("REFERENCES public.courses(id) MATCH SIMPLE                         ");
            sb.Append("ON UPDATE NO ACTION                                                ");
            sb.Append("ON DELETE NO ACTION                                                ");
            sb.Append("NOT VALID,                                                         ");
            sb.Append("    CONSTRAINT student_id_key FOREIGN KEY(student_id)              ");
            sb.Append("REFERENCES public.students(id) MATCH SIMPLE                        ");
            sb.Append("ON UPDATE NO ACTION                                                ");
            sb.Append("ON DELETE NO ACTION                                                ");
            sb.Append(")                                                                  ");
            sb.Append("                                                                   ");
            sb.Append("TABLESPACE pg_default;                                             ");
            sb.Append("                                                                   ");
            sb.Append("ALTER TABLE public.cours_scheme                                    ");
            sb.Append(@"    OWNER to ""OTUSUser"";                                   ");
            sb.Append("                                                                   ");
            sb.Append("INSERT INTO public.cours_scheme(student_id, course_id) VALUES(1,1);");
            sb.Append("INSERT INTO public.cours_scheme(student_id, course_id) VALUES(2,1);");
            sb.Append("INSERT INTO public.cours_scheme(student_id, course_id) VALUES(3,1);");
            sb.Append("INSERT INTO public.cours_scheme(student_id, course_id) VALUES(4,1);");
            sb.Append("INSERT INTO public.cours_scheme(student_id, course_id) VALUES(5,1);");
            sb.Append("INSERT INTO public.cours_scheme(student_id, course_id) VALUES(6,3);");

            var commandText = sb.ToString();
            _proxy.SendCommand(commandText);
        }
        

        public override void ReadAll(Action<string> showData)
        {
            var tableName = "cours_scheme";
            ReadTable(tableName,
                () =>
                {
                    showData($"guid\tstudent_id\tcourse_id");
                },
                data =>
                {
                    showData(
                        $"{string.Join(Environment.NewLine, data)}");
                });
        }

        public override void Add(Func<string, string> GetData)
        {
            if (!long.TryParse(GetData("Enter student id"), out var sid))
                throw new ArgumentException($"Unknown student id!");

            if (!long.TryParse(GetData("Enter course id"), out var cid))
                throw new ArgumentException($"Unknown course id!");

            var paramList = new List<(string name, object value)>
            {
                new("STUDENT_ID,", sid),
                new("COURSE_ID", cid),
            };

            var commandText = "INSERT INTO public.cours_scheme(student_id, course_id) ";
            _proxy.SendCommand(commandText, paramList);
        }
    }
}