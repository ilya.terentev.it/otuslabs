﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;
using NpgsqlTypes;

namespace HomeWorkPSQLConsole
{
    public class CourseTable :TableBase
    {
        public CourseTable(string name, IProxy proxy) : base(name, proxy)
        {
        }

        public override void Init()
        {
             var sb = new StringBuilder();
             sb.Append("CREATE SEQUENCE public.course_id_seq    ");
             sb.Append("INCREMENT 1                              ");
             sb.Append("START 1                                  ");
             sb.Append("MINVALUE 1;                               ");
             sb.Append("ALTER SEQUENCE public.course_id_seq     ");
             sb.Append(@"    OWNER TO ""OTUSUser"";              ");
             sb.Append("CREATE TABLE IF NOT EXISTS public.courses                                   ");
             sb.Append("(                                                                           ");
             sb.Append(@"    id bigint NOT NULL DEFAULT nextval('course_id_seq'::regclass),         ");
             sb.Append(@"    name character varying COLLATE pg_catalog.""default"" NOT NULL,           ");
             sb.Append(@"    CONSTRAINT ""Courses_pkey"" PRIMARY KEY (id)                              ");
             sb.Append(")                                                                           ");
             sb.Append("                                                                            ");
             sb.Append("TABLESPACE pg_default;                                                      ");
             sb.Append("                                                                            ");
             sb.Append("ALTER TABLE public.courses                                                  ");
             sb.Append(@"    OWNER to ""OTUSUser"";                                              ");
             sb.Append("	                                                                        ");
             sb.Append("INSERT INTO public.courses(name) VALUES ('C#_pro');                         ");
             sb.Append("INSERT INTO public.courses(name) VALUES ('Java_junior');                    ");
             sb.Append("INSERT INTO public.courses(name) VALUES ('Web');                            ");
             sb.Append("INSERT INTO public.courses(name) VALUES ('DevOps');                         ");

             var commandText = sb.ToString();
             _proxy.SendCommand(commandText);
        }

        public override void ReadAll(Action<string> showData)
        {
            var tableName = "courses";
            ReadTable(tableName,
                () =>
                {
                    showData($"id\tname ");
                },
                data =>
                {
                    showData(
                        $"{string.Join(Environment.NewLine, data)}");
                });
        }


        public override void Add(Func<string, string> GetData)
        {
            var paramList = new List<(string name, object value)>
            {
                new("NAME", GetData("Enter name"))
            };

            var commandText = "INSERT INTO public.courses(name)";
            _proxy.SendCommand(commandText, paramList);
        }
    }
}