﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;

namespace HomeWorkPSQLConsole
{
    public class PSqlProxy:IProxy
    {
        private readonly string _cs;
        public PSqlProxy(string cs)
        {
            _cs = cs;
        }
        public string TestConnection()
        {
            using var con = new NpgsqlConnection(_cs);
            con.Open();
            const string sql = "SELECT version()";
            using var cmd = new NpgsqlCommand(sql, con);
            var version = cmd.ExecuteScalar()?.ToString();
            return $"PostgreSQL version: {version}";
        }
        public void SendCommand(string cmd)
        {
            using var con = new NpgsqlConnection(_cs);
            con.Open();
            var command = new NpgsqlCommand(cmd, con);
            command.ExecuteNonQuery();
        }
        public void SendCommand(string cmd, List<(string name, object value)> parameters)
        {
            using var con = new NpgsqlConnection(_cs);
            con.Open();
            cmd += $" VALUES(@{string.Join(", @", parameters.Select(x=>x.name))});";
            var command = new NpgsqlCommand(cmd, con);
            for (var i = 0; i < parameters.Count; i++)
            {
                command.Parameters.Add($"@{parameters[i].name}", parameters[i].value.GetNpgsqlDbType());
                command.Parameters.Last().Value = parameters[i].value;
            }

            command.ExecuteNonQuery();
        }
        public List<string> GetData(string sqlQuery)
        {
            using var con = new NpgsqlConnection(_cs);
            con.Open();
            using var cmd = new NpgsqlCommand(sqlQuery, con);
            using var rdr = cmd.ExecuteReader();
            var rows = new List<string>();
            while (rdr.Read())
            {
                var res = new List<object>();
                for (var i = 0; i < rdr.GetColumnSchema().Count; i++)
                {
                    FillValue(rdr, i, res);
                }
                rows.Add(string.Join("\t", res));
            }

            return rows;
        }

        private static void FillValue(NpgsqlDataReader rdr, int i, List<object> res)
        {
            var typeName = rdr.GetDataTypeName(i);
            switch (typeName)
            {
                case "bigint":
                case "Int64":
                    res.Add(rdr.GetInt64(i));
                    break;
                case "character varying":
                case "String":
                    res.Add(rdr.GetString(i));
                    break;
                case "uuid":
                    res.Add(rdr.GetGuid(i));
                    break;
                default:
                    throw new ArgumentException("Unknown type");
            }
        }
    }
}
