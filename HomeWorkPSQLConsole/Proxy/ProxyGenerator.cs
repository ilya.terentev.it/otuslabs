﻿namespace HomeWorkPSQLConsole
{
    public static class ProxyGenerator
    {
        private static PSqlProxy _proxy;
        public static IProxy GetProxy(string cs)
        {
            return _proxy ?? new PSqlProxy(cs);
        }
    }
}
