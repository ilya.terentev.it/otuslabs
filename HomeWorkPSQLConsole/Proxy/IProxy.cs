﻿using System.Collections.Generic;

namespace HomeWorkPSQLConsole
{
    public interface IProxy
    {
        List<string> GetData(string sqlQuery);
        string TestConnection();
        void SendCommand(string cmd);
        void SendCommand(string cmd, List<(string name, object value)> parameters);
    }
}
