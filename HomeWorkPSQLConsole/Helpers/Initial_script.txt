CREATE SEQUENCE public.student_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 0;

ALTER SEQUENCE public.student_id_seq
    OWNER TO "OTUSUser";

CREATE TABLE IF NOT EXISTS public.students
(
    id bigint NOT NULL DEFAULT nextval('student_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    mid_name character varying COLLATE pg_catalog."default",
    surname character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "Students_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.students
    OWNER to "OTUSUser";

CREATE TABLE IF NOT EXISTS public.courses
(
    id bigint NOT NULL DEFAULT nextval('course_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "Courses_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.courses
    OWNER to "OTUSUser";
	
CREATE TABLE IF NOT EXISTS public.cours_scheme
(
    guid uuid NOT NULL DEFAULT gen_random_uuid(),
    student_id bigint NOT NULL,
    course_id bigint NOT NULL,
    CONSTRAINT cours_scheme_pkey PRIMARY KEY (guid),
    CONSTRAINT course_id_key FOREIGN KEY (course_id)
        REFERENCES public.courses (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT student_id_key FOREIGN KEY (student_id)
        REFERENCES public.students (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.cours_scheme
    OWNER to "OTUSUser";
	
INSERT INTO public.students(name, mid_name, surname) VALUES ('Bob', 'Ivanovich', 'Black');
INSERT INTO public.students(name, mid_name, surname) VALUES ('Clark', 'Ivanovich', 'White');
INSERT INTO public.students(name, mid_name, surname) VALUES ('Addy', 'Ivanovich', 'Red');
INSERT INTO public.students(name, mid_name, surname) VALUES ('Mindy', 'Alexandrovna', 'Yellow');
INSERT INTO public.students(name, mid_name, surname) VALUES ('Bobby', 'Ivanovich', 'Green');
INSERT INTO public.students(name, mid_name, surname) VALUES ('BobbyJ', 'Ivanovich', 'Smit');

INSERT INTO public.courses(name) VALUES ('C#_pro');
INSERT INTO public.courses(name) VALUES ('Java_junior');
INSERT INTO public.courses(name) VALUES ('Web');
INSERT INTO public.courses(name) VALUES ('DevOps');

INSERT INTO public.cours_scheme(student_id, course_id) VALUES (1,1);
INSERT INTO public.cours_scheme(student_id, course_id) VALUES (2,1);
INSERT INTO public.cours_scheme(student_id, course_id) VALUES (3,1);
INSERT INTO public.cours_scheme(student_id, course_id) VALUES (4,1);
INSERT INTO public.cours_scheme(student_id, course_id) VALUES (5,1);
INSERT INTO public.cours_scheme(student_id, course_id) VALUES (6,3);