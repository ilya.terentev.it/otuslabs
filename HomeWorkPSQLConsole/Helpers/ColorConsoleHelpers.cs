﻿using System;

namespace HomeWorkPSQLConsole
{
    public static class ColorConsole
    {
        public static ConsoleColor _defColor = Console.ForegroundColor;
        public static void WriteLine(string str, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(str);
            Console.ForegroundColor = _defColor;
        }
    }
}
