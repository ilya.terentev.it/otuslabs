﻿using System;
using NpgsqlTypes;

namespace HomeWorkPSQLConsole
{
    public static class TypeExtensions
    {
        public static NpgsqlDbType GetNpgsqlDbType(this object o)
        {
            switch (o)
            {
                case long:
                    return NpgsqlDbType.Bigint;
                case string:
                    return NpgsqlDbType.Varchar;
                default:
                    throw new AggregateException("Unknown type");

            }
        }
    }
}