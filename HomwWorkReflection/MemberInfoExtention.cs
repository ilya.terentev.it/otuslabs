﻿using System;
using System.Reflection;

namespace HomeWorkReflection
{
    public static class MemberInfoExtention
    {
        public static Type GetUnderlyingType(this MemberInfo member)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Event:
                    return ((EventInfo) member).EventHandlerType;
                case MemberTypes.Field:
                    return ((FieldInfo) member).FieldType;
                case MemberTypes.Method:
                    return ((MethodInfo) member).ReturnType;
                case MemberTypes.Property:
                    return ((PropertyInfo) member).PropertyType;
                default:
                    throw new ArgumentException
                    (
                        "Input MemberInfo must be type of EventInfo, FieldInfo, MethodInfo, or PropertyInfo"
                    );
            }
        }

        public static void SetValue<T>(this MemberInfo member, T t, object val)
        {
            switch (member)
            {
                case PropertyInfo pi when !pi.CanWrite:
                    //ignore
                    break;
                case PropertyInfo pi when pi.CanWrite:
                    pi.SetValue(t, val);
                    break;
                case FieldInfo fi:
                    fi.SetValue(t, val);
                    break;
                default:
                    throw new ArgumentException
                    (
                        "Input MemberInfo must be type of FieldInfo or PropertyInfo"
                    );
            }
        }

        public static object GetValue<T>(this MemberInfo member, T t, Settings settings)
        {
            object res;
            switch (member)
            {
                case PropertyInfo pi when !pi.CanRead:
                    return null;
                case PropertyInfo pi when pi.CanRead:
                    res = pi.GetValue(t);
                    break;
                case FieldInfo fi:
                    res = fi.GetValue(t);
                    break;
                default:
                    throw new ArgumentException
                    (
                        "Input MemberInfo must be type of FieldInfo or PropertyInfo"
                    );
            }

            if (res is string s)
            {
                if (s.Contains(settings.CsvFieldSeparator) || s.Contains(settings.NameValueSeparator))
                    throw new ArgumentException
                    (
                        $"String member must not contain special characters ('{settings.CsvFieldSeparator}', '{settings.NameValueSeparator}')!"
                    );
            }

            return res;
        }

        public static bool IsSimple(this Type type)
        {
            return type.IsPrimitive
                   || type.IsEnum
                   || type == typeof(string)
                   || type == typeof(decimal);
        }
    }
}