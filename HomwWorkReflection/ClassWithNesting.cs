﻿using System;
using System.Collections.Generic;

namespace HomwWorkReflection
{
    public static class ClassGenerator
    {
        public static ClassWithNesting GetNewInstance
        {
            get
            {
                var _r = new Random();
                return new ClassWithNesting
                {
                    PublicString = "StringTest",
                    i1 = _r.Next(100),
                    decimal1 = 123,
                    i2 = _r.Next(100),
                    i3 = _r.Next(100),
                    i4 = _r.Next(100),
                    i5 = _r.Next(100),
                    pubString1 = "string1",
                    pubString2 = "string2",
                    pubString3 = "string3",
                    b = new B
                    {
                        d1 = _r.NextDouble() * 100,
                        c = new C
                        {
                            i33 = _r.Next(100),
                            Enum1 = DigitEnum.Two,
                            DoubleList = new List<double> {0d, 1d, 2d}
                        },
                        LitOfListString = new List<List<string>> {new() {"123"}},
                        LitOfD = new List<D> {new()}
                    }
                };
            }
        }
    }

    [Serializable]
    public class ClassWithNesting
    {
      
        public static string StaticString = "DefaultStaticString";
        private float _f1 = 9;
        private string _privateString = "PrivatePropString";
        private string _privateString2 = "PrivatePropString2";
        public B b;
        public decimal decimal1;
        public int i1, i2, i3, i4, i5;
        public string pubString1, pubString2, pubString3;

        public virtual string PublicString
        {
            get => _privateString;
            set => _privateString = value;
        }
    }

    [Serializable]
    public class B
    {
        public B()
        {
            
        }
        public C c = new C();
        public double d1;
        public List<D> LitOfD = new();
        public List<List<string>> LitOfListString = new();
    }

    [Serializable]
    public class C
    {
        public List<double> DoubleList = new();
        public List<double> DoubleList2 = new();
        public DigitEnum Enum0;
        public DigitEnum Enum1;

        public int i33;
    }

    [Serializable]
    public class A
    {
        public A child;
    }

    [Serializable]
    public class D
    {
        public string Name => "Dname";
    }

    [Serializable]
    [Flags]
    public enum DigitEnum
    {
        None = 0,
        One = 1,
        Two = 2,
        Three = One + Two
    }
}