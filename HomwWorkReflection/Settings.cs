﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace HomeWorkReflection
{
    public class Settings
    {
        public Settings(string csvFieldSeparator, string nameValueSeparator)
        {
            CsvFieldSeparator = csvFieldSeparator;
            NameValueSeparator = nameValueSeparator;
        }
        public string CsvFieldSeparator { get; private set; } 
        public string NameValueSeparator { get; private set; }
    }
}
