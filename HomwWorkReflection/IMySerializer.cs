﻿namespace HomwWorkReflection
{
    public  interface IMySerializer
    {
        public string Serialize(object o);
        public T Deserialize<T>(string str);
    }
}
