﻿
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using HomeWorkReflection;

namespace HomwWorkReflection
{
    internal class Program
    {
        private static void Main(string[] args) 
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            var csvFieldSeparator = configuration.GetSection("MySerializerSettings").GetValue<string>("CsvFieldSeparator");
            var nameValueSeparator = configuration.GetSection("MySerializerSettings").GetValue<string>("NameValueSeparator");
            var settings = new Settings(csvFieldSeparator, nameValueSeparator);

            var defaultF = new DefaultClass().Get();
            var serializers = new List<IMySerializer>
            {
                new DotNetXmlSerializer(), new NewtonSerializer(), new MySerializer(settings), new DotNetJsonSerializer()
            };
            var iterationNumber = 10_000;
            var nameColor = ConsoleColor.Yellow;
            var instColor = ConsoleColor.Gray;
            var resultColor = ConsoleColor.White;
            Console.WriteLine($"{defaultF.GetType().Name} {iterationNumber} iterations", Console.ForegroundColor = nameColor);
            foreach (var serializer in serializers)
            {
                Console.WriteLine($"{serializer.GetType().Name}", Console.ForegroundColor = nameColor);
                Console.ForegroundColor = instColor;
                TestSerializer(serializer, defaultF, iterationNumber, resultColor);
                TestDeserializer(serializer, defaultF, iterationNumber, resultColor);
            }

            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("********");
            Console.WriteLine("*******************");
            Console.WriteLine("********");
            Console.WriteLine();

            var fWtihNesting = ClassGenerator.GetNewInstance;
            Console.WriteLine($"{fWtihNesting.GetType().Name} {iterationNumber} iterations ", Console.ForegroundColor = nameColor);
            foreach (var serializer in serializers)
            {
                Console.WriteLine($"{serializer.GetType().Name}", Console.ForegroundColor = nameColor);
                Console.ForegroundColor = instColor;
                TestSerializer(serializer, fWtihNesting, iterationNumber, resultColor);
                TestDeserializer(serializer, fWtihNesting, iterationNumber, resultColor);
            }
        }

        private static void TestSerializer<T>(IMySerializer mySerializer, T tInstance, int iterationNumber = 100_000,
            ConsoleColor color = ConsoleColor.White)
        {
            var start = DateTime.UtcNow;
            for (var i = 0; i < iterationNumber; i++)
                _ = mySerializer.Serialize(tInstance);
            var ms = (DateTime.UtcNow - start).TotalMilliseconds;
            Console.WriteLine($"{mySerializer.Serialize(tInstance)}");
            Console.WriteLine($"\t serialize time: {ms:F3} ms",
                Console.ForegroundColor = ConsoleColor.White);
        }

        private static void TestDeserializer<T>(IMySerializer serializer, T tInstance, int iterationNumber = 100_000,
            ConsoleColor color = ConsoleColor.White)
        {
            var a = serializer.Serialize(tInstance);
            var start = DateTime.UtcNow;
            for (var i = 0; i < iterationNumber; i++)
                _ = serializer.Deserialize<T>(a);
            Console.WriteLine(
                $"\t deserialize time: {(DateTime.UtcNow - start).TotalMilliseconds:F3} ms",
                Console.ForegroundColor = color);
        }
    }
}