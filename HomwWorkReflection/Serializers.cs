﻿using System.IO;
using System.Text.Json;
using System.Xml.Serialization;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace HomwWorkReflection
{
    public class NewtonSerializer : IMySerializer
    {
        public string Serialize(object o)
        {
            return JsonConvert.SerializeObject(o);
        }

        public T Deserialize<T>(string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
    }

    public class DotNetJsonSerializer : IMySerializer
    {
        private static readonly JsonSerializerOptions options = new() {WriteIndented = true, IncludeFields = true};

        public string Serialize(object o)
        {
            return JsonSerializer.Serialize(o, o.GetType(), options);
        }

        public T Deserialize<T>(string str)
        {
            return JsonSerializer.Deserialize<T>(str);
        }
    }

    public class DotNetXmlSerializer : IMySerializer
    {
        public string Serialize(object o)
        {
            var xmlSerializer = new XmlSerializer(o.GetType());
            using var textWriter = new StringWriter();
            xmlSerializer.Serialize(textWriter, o);
            return textWriter.ToString();
        }

        public T Deserialize<T>(string str)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            using var reader = new StringReader(str);
            return (T) xmlSerializer.Deserialize(reader);
        }
    }
}