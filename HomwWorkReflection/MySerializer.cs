﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using HomeWorkReflection;

namespace HomwWorkReflection
{
    public class MySerializer : IMySerializer
    {
        static BindingFlags BindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
        private readonly Settings _settings;

        public MySerializer(Settings settings)
        {
            _settings = settings;
        }

        public string Serialize(object o)
        {
            var t = DisassembleType(o, o.GetType());
            var sb = new StringBuilder();
            foreach (var tuple in t)
            {
                for (var i = 0; i < tuple.nestingNumber; i++)
                    sb.Append(_settings.CsvFieldSeparator);
                sb.AppendLine($"{tuple.Name}|{tuple.Value}");
            }

            return sb.ToString();
        }

        public T Deserialize<T>(string str)
        {
            var rows = str.Split(Environment.NewLine);
            var dic = ParseCsvString<T>(rows);
            var t = typeof(T) == typeof(string) ? "" : Activator.CreateInstance(typeof(T));
            var startind = t.GetType().IsGenericType ? 3 : 1;
            var res = FillType(t, dic, startind);

            if (res is T tRes)
                return tRes;
            throw new InvalidOperationException($"Unknown class {res.GetType().Name}, awaited {typeof(T).GetType().Name}");
        }


        #region Serialize staff

        private List<(string Name, string Value, int nestingNumber)> DisassembleType(object o, Type t,
            int numberOfNests = 0,
            string memberName = "")
        {
            var res = new List<(string Name, string Value, int nestingNumber)>();
            if (numberOfNests == 0)
            {
                if (t.IsSimple())
                {
                    res.Add(("", $"{o}", numberOfNests));
                    return res;
                }

                if (!t.IsGenericType)
                {
                    res.Add(($"{o.GetType().Name}", "", numberOfNests));
                }
                numberOfNests++;
            }

            if (o.GetType().IsGenericType)
            {
                var j = 0;
                res.Add(($"{memberName}", "", numberOfNests));
                numberOfNests++;
                foreach (var v in (IEnumerable) o)
                    if (v.GetType().IsSimple())
                    {
                        res.Add(($"item{j++}", $"{v}", numberOfNests));
                    }
                    else
                    {
                        if (v.GetType().IsClass && !v.GetType().IsGenericType)
                        {
                            res.Add(($"{v.GetType().Name}", "", numberOfNests));
                            numberOfNests++;
                        }

                        if (o == null)
                            res.Add(($"item{j++}", $"{o}", numberOfNests));
                        else
                            res.AddRange(DisassembleType(v, v.GetType(), numberOfNests, $"item{j++}"));
                    }

                return res;
            }

            var fields = t.GetFields(BindingFlags);
            foreach (var field in fields)
                DisassembleMember(o, t, field, res, numberOfNests);

            foreach (var prop in t.GetProperties(BindingFlags))
                DisassembleMember(o, t, prop, res, numberOfNests);

            return res;
        }

        private void DisassembleMember(object o, Type t, MemberInfo member,
            List<(string Name, string Value, int nestingNumber)> res,
            int numberOfNests)
        {
            if (member is FieldInfo fi)
                if ((fi.Attributes & FieldAttributes.Private) != 0 ||
                    (fi.Attributes & FieldAttributes.Static) != 0)
                    return;

            if (member.GetUnderlyingType() == t)
                throw new InvalidOperationException($"Can't serialize! Infinite recursion ({member.GetUnderlyingType().Name})!");

            if (member.GetUnderlyingType().IsSimple())
            {
                res.Add(($"{member.Name}", $"{member.GetValue(o, _settings)}", numberOfNests));
            }
            else
            {
                var oo = member.GetUnderlyingType().IsClass ? member.GetValue(o, _settings) : o;

                if (member.GetUnderlyingType().IsClass && !member.GetUnderlyingType().IsGenericType && oo != null)
                {
                    res.Add(($"{member.Name}", "", numberOfNests));
                    numberOfNests++;
                }

                if (oo != null)
                {
                    if (member.GetUnderlyingType().IsGenericType)
                        res.AddRange(DisassembleType(oo, member.GetUnderlyingType(), numberOfNests, member.Name));
                    else
                        res.AddRange(DisassembleType(oo, member.GetUnderlyingType(), numberOfNests));
                }
            }
        }

        #endregion

        #region Deserialize staff

        private List<(string Name, string Value, int nestingNumber)> ParseCsvString<T>(string[] rows)
        {
            var dic = new List<(string Name, string Value, int nestingNumber)>();
            for (var i = 0; i < rows.Length; i++)
            {
                if (string.IsNullOrEmpty(rows[i]))
                    continue;
                var lngth = rows[i].Split(_settings.CsvFieldSeparator);
                var val = lngth.Where(x => !string.IsNullOrEmpty(x));
                if (val.Count() != 1)
                    throw new ArgumentException("Wrong string!");
                var sval = val.First().Split(_settings.NameValueSeparator);
                if (sval.Count() != 2)
                    throw new ArgumentException("Wrong string!");
                dic.Add((sval[0], sval[1], lngth.Length - 1));
            }

            return dic;
        }

        private object FillType(object t, List<(string Name, string Value, int nestingNumber)> lt, int numberOfNests,
            int i = 0)
        {
            if (t.GetType().IsSimple())
            {
                var converter = TypeDescriptor.GetConverter(t.GetType());
                var val = converter.ConvertFromString(lt[i].Value);
                return val;
            }

            if (t.GetType().IsGenericType)
            {
                var listInst = (IList) Activator.CreateInstance(t.GetType());
                var nestNumber = numberOfNests - 1;
                while (lt[++i].nestingNumber == nestNumber)
                {
                    var tt = t.GetType().GenericTypeArguments.First();
                    if (tt.IsSimple())
                    {
                        var str = lt[i].Value;
                        var converter = TypeDescriptor.GetConverter(tt);
                        listInst.Add(converter.ConvertFromString(str));
                    }
                    else
                    {
                        var inst = Activator.CreateInstance(tt);
                        if (inst.GetType().IsGenericType)
                            numberOfNests++;
                        var filledType = FillType(inst, lt, numberOfNests, i);
                        listInst.Add(filledType);
                    }

                    if (lt.Count == i+1)
                        break;
                }

                return listInst;
            }

            foreach (var field in t.GetType().GetFields(BindingFlags))
                FillMember(t, lt, field, numberOfNests);
            foreach (var prop in t.GetType().GetProperties(BindingFlags))
                FillMember(t, lt, prop, numberOfNests);
            return t;
        }

        private void FillMember(object t, List<(string Name, string Value, int nestingNumber)> lt, MemberInfo member,
            int numberOfNests = 0)
        {
            if (member.GetUnderlyingType() == t.GetType())
                throw new InvalidOperationException($"Can't serialize! Infinite recursion ({member.GetUnderlyingType().Name})!");
            if (member.GetUnderlyingType().IsSimple() || !member.GetUnderlyingType().IsClass)
            {
                var ltItem = lt.FirstOrDefault(x => x.nestingNumber == numberOfNests && x.Name == $"{member.Name}");
                SetValue(t, member, ltItem.Value);
            }
            else
            {
                var rType = TypeDescriptor.GetReflectionType(member.GetUnderlyingType());
                var inst = Activator.CreateInstance(rType);

                var ltItem = lt.FirstOrDefault(x => x.nestingNumber == numberOfNests && x.Name == $"{member.Name}");
                if (ltItem != default)
                {
                    var i = lt.IndexOf(ltItem);
                    if (member.GetUnderlyingType().IsGenericType)
                        numberOfNests++;

                    var filledType = FillType(inst, lt, numberOfNests + 1, i);
                    member.SetValue(t, filledType);
                }
            }
        }

        private static void SetValue<T>(T t, MemberInfo member, string str)
        {
            if (str != null)
            {
                var converter = TypeDescriptor.GetConverter(member.GetUnderlyingType());
                var val = converter.ConvertFromString(str);
                member.SetValue(t, val);
            }
            else
            {
                member.SetValue(t, null);
            }
        }

        #endregion
    }
}