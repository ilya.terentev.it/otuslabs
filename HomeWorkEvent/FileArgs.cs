﻿using System;

namespace HomeWorkEvent
{
    public class FileArgs : EventArgs
    {
        public FileArgs(string fileName)
        {
            FileName = fileName;
        }
        public string FileName { get; private set; }
    }
}