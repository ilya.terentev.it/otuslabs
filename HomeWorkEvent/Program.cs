﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace HomeWorkEvent
{
    class Program
    {
        static void Main(string[] args)
        {
            const string path = @"Test";
            var tcs = new CancellationTokenSource();
            var fs = new FileSearcher(path);
            fs.FileFound += Fs_FileFound;
            Console.WriteLine("Press ESC to stop");
            //Task.Run(() => { fs?.Start(path, tcs.Token); }, tcs.Token);
            
            Console.WriteLine($"Max:{fs.GetFileNames(tcs.Token).GetMax(s => string.IsNullOrEmpty(s) ? 0 : s.Length)}");
            while (Console.ReadKey().Key != ConsoleKey.Escape)
            {
            }
            tcs.Cancel();
            fs.FileFound -= Fs_FileFound;
        }

        private static readonly List<string> _fileNames = new();
        private static void Fs_FileFound(object sender, FileArgs e)
        {
            _fileNames.Add(e.FileName);
            var max = _fileNames.GetMax(s => string.IsNullOrEmpty(s) ? 0 : s.Length);
            Console.WriteLine($"{e.FileName} found! Max:{max}");
        }
    }
}
