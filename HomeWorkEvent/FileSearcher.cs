﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HomeWorkEvent
{
    public class FileSearcher
    {
        private string _directory;
        public FileSearcher(string directory)
        {
            _directory = directory;
        }
        public event EventHandler<FileArgs> FileFound;

        public IEnumerable<string> GetFileNames(CancellationToken token = default)
        {
            foreach (var file in Directory.GetFiles(_directory))
            {
                if (token.IsCancellationRequested)
                    yield break;
                FileFound?.Invoke(this, new FileArgs(file));
                yield return file;
            }
        }

        public void Start(string directory, CancellationToken token)
        {
            var foundedFiles = new List<string>();
            while (!token.IsCancellationRequested)
            {
                var files = Directory.GetFiles(directory);
                foreach (var file in files)
                {
                    if (token.IsCancellationRequested)
                        return;
                    if(foundedFiles.All(x => x != file))
                        FileFound?.Invoke(this, new FileArgs(Path.GetFileName(file)));
                    foundedFiles.Add(file);
                }
            }
        }
    }
}
