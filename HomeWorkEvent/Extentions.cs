﻿using System;
using System.Collections.Generic;

namespace HomeWorkEvent
{
    public static class Extensions
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
        {
            T max = default;
            foreach (var v in e)
            {
                var a = getParameter(max);
                var b = getParameter(v);
                max = a > b ? max : v;
            }
            return max;
        }
    }
}
