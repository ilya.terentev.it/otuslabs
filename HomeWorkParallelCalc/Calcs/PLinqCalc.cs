﻿using System.Collections.Generic;
using System.Linq;

namespace HomeWorkParallelCalc.Calcs
{
    public class PLinqCalc : ICalc
    {
        private readonly IEnumerable<long> _data;
        public PLinqCalc(IEnumerable<int> data)
        {
            Name = "Plinq   ";
            _data = data.Select(x=>(long)x);
        }

        public string Name { get; }

        public long Calc()
        {
            return _data.AsParallel().Sum();
        }
    }
}