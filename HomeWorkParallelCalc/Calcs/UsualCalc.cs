﻿namespace HomeWorkParallelCalc.Calcs
{
    public class UsualCalc : ICalc
    {
        private int[] _data;
        public UsualCalc(int[] data)
        {
            Name = "Usual   ";
            _data = data;
        }

        public string Name { get; }

        public long Calc()
        {
            long sum = 0;
            foreach (var d in _data)
                sum += d;

            return sum;
        }
    }
}