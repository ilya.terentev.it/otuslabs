﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace HomeWorkParallelCalc.Calcs
{
    public class ThreadsCalc : ICalc
    {
        private int _threadCount;
        private int[] _data;
        private long _result;
        public ThreadsCalc(int[] data, int threadCount)
        {
            Name = $"In {threadCount} thread";
            _data = data;
            _threadCount = threadCount;
        }

        public string Name { get; }
        private object _lock = new();
        public long Calc()
        {
            _result = 0;
            var count = _data.Length / _threadCount;
            var doneEvents = new List<ManualResetEvent>();
            for (var i = 0; i < _data.Length; i += count)
            {
                var e = new ManualResetEvent(false);
                doneEvents.Add(e);
                var lngs = Math.Min(count, _data.Length - i);
                var p = new Tuple<int[], int, int, ManualResetEvent>(_data, i, lngs, e);
                var t = new Thread(CalcSum);
                t.Start(p);
            }

            WaitHandle.WaitAll(doneEvents.ToArray());
            return _result;
        }

        private void CalcSum(object? o)
        {
            if (o is not Tuple<int[], int, int, ManualResetEvent> tt) 
                return;
            long sum = 0;
            for (var i = tt.Item2; i < tt.Item3; i++)
                sum += tt.Item1[i];

            lock (_lock)
            {
                _result += sum;
            }

            tt.Item4.Set();
        }
    }
}