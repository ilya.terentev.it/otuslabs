﻿using System.Collections.Generic;
using System.Linq;

namespace HomeWorkParallelCalc.Calcs
{
    public class LinqCalc:ICalc
    {
        private IEnumerable<long> _data;
        public LinqCalc(IEnumerable<int> data)
        {
            Name = "Linq    ";
            _data = data.Select(x=>(long)x);
        }

        public string Name { get; }

        public long Calc()
        {
            return _data.Sum();
        }
    }
}