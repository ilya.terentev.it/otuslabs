﻿namespace HomeWorkParallelCalc.Calcs
{
    public interface ICalc
    {
        public string Name { get; }
        public long Calc();
    }
}
