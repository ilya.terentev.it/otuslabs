﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using HomeWorkParallelCalc.Calcs;

namespace HomeWorkParallelCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            //Замерьте время выполнения для 100 000, 1 000 000 и 10 000 000
            Calc(100_000);
            Console.WriteLine("==============");
            Calc(1_000_000);
            Console.WriteLine("==============");
            Calc(10_000_000);
            Console.WriteLine("==============");
            Calc(100_000_000);
            Console.WriteLine("==============");
            Console.ReadKey();
        }

        private static void Calc(int count)
        {
            var array = new int[count];
            var rand = new Random();
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(0, 100);
            }

            var calcs = new List<ICalc>()
            {
                new LinqCalc(array),
                new PLinqCalc(array),
                new UsualCalc(array),
                new ThreadsCalc(array, 2),
                new ThreadsCalc(array, 4)
            };

            Console.WriteLine($"{count:N0} records");
            foreach (var calc in calcs)
            {
                var sw = new Stopwatch();
                sw.Start();
                var r = calc.Calc();
                sw.Stop();
                Console.WriteLine($"{calc.Name} \tsum {r} \t{sw.Elapsed.TotalMilliseconds:F2}ms");
            }
        }
    }
}
