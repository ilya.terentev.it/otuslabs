﻿using System;
using System.Diagnostics;

namespace HomeWorkWhatsNewCsharp9
{
    public  class ColoredConsole : IUserCommunication
    {
        private static ConsoleColor _errorColor = ConsoleColor.Black;
        private static ConsoleColor _questionColor = ConsoleColor.Black;
        private static ConsoleColor _infoColor = ConsoleColor.Black;

        public ColoredConsole()
        {
            //NEW FEATURE C#9 "Attributes on local functions"
            [Conditional("DEBUG")]
            static void ChangeColors()
            {
                _errorColor = ConsoleColor.DarkRed;
                _questionColor = ConsoleColor.Green;
                _infoColor = ConsoleColor.Yellow;
            }
            [Conditional("NotWorkedConditionalForTest")]
            static void ChangeColors2()
            {
                _errorColor = ConsoleColor.Cyan;
                _questionColor = ConsoleColor.DarkMagenta;
                _infoColor = ConsoleColor.White;
            }

            ChangeColors();
            ChangeColors2();
        }
        public void ShowError(string msg)
        {
            ColoredWriteLine(msg, _errorColor);
        }

        public void AskQuestion(string msg)
        {
            ColoredWriteLine(msg, _questionColor);
        }

        private void ColoredWriteLine(string msg, ConsoleColor color)
        {
            var oldColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(msg);
            Console.ForegroundColor = oldColor;
        }

        public bool WaitReaction(string msg, ref bool exit)
        {
            ColoredWriteLine(msg, _infoColor);
            Console.WriteLine("Press any key for continue, ESC to stop");
            if (Console.ReadKey(true).Key == ConsoleKey.Escape)
            {
                exit = true;
                return true;
            }

            return false;
        }

        public void Clear(string msg = "")
        {
            Console.Clear();
            Console.WriteLine("Press ESC to stop");
            if (!string.IsNullOrEmpty(msg))
                AskQuestion(msg);
        }

        public bool WaitKey(ref string str, out bool @continue)
        {
            while (!Console.KeyAvailable)
            {
            }

            var keyInfo = Console.ReadKey(true);
            @continue = false;

            if (keyInfo.Key == ConsoleKey.Escape)
            {
                str = "";
                return true;
            }

            str += keyInfo.KeyChar;

            if (keyInfo.Key != ConsoleKey.Enter)
            {
                Console.Write(keyInfo.KeyChar);
                @continue = true;
                return false;
            }

            Console.WriteLine();

            return false;
        }
    }
}