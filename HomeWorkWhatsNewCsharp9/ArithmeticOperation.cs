﻿namespace HomeWorkWhatsNewCsharp9
{
    //NEW FEATURE  C#9  "Records" 
    public record ArithmeticOperation
    {
        //NEW FEATURE  C#9  "Init only setters" 
        //NEW FEATURE  C#9  "Native-sized integers" 
        public nint FirstNumber { get; init; }
        public nint SecondNumber { get; init; }
        public OperationTypes OperationType { get; init; }
    }
}