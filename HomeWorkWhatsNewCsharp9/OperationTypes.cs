﻿namespace HomeWorkWhatsNewCsharp9
{
    public enum OperationTypes
    {
        Multiplication,
        Division,
        Sum,
        Difference
    }
}