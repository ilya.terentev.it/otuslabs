﻿using System;

namespace HomeWorkWhatsNewCsharp9
{
    public class SimpleConsoleCalculator
    {
        public SimpleConsoleCalculator(IUserCommunication userCommunicator)
        {
            _userCommunicator = userCommunicator;
        }

        public void Run()
        {
            var action = Action.GetFirstNumber;
            var exit = false;
            var str = "";
            _userCommunicator.Clear("insert first digit");
            do
            {
                try
                {
                    if (_userCommunicator.WaitKey(ref str, out var c))
                        break;
                    if (c)
                        continue;
                    _userCommunicator.Clear();

                    action = DoAction(action, str, ref exit);
                }
                catch
                {
                    _userCommunicator.ShowError("Ay yai yai!");
                    _userCommunicator.AskQuestion("insert first digit");
                    action = Action.GetFirstNumber;
                }

                str = "";

            } while (!exit);
        }

        private readonly IUserCommunication _userCommunicator;
        private nint _firstNumber;
        private nint _secondNumber;

        private Action DoAction(Action action, string str, ref bool exit)
        {
            switch (action)
            {
                case Action.GetFirstNumber:
                    action = ParseNumber(action, str, ref _firstNumber);
                    break;
                case Action.GetSecondNumber:
                    action = ParseNumber(action, str, ref _secondNumber);
                    break;
                case Action.SelectAction:
                    //NEW FEATURE C#9 "Target-typed new expressions"
                    ArithmeticOperation o = new()
                    {
                        FirstNumber = _firstNumber,
                        SecondNumber = _secondNumber,
                        OperationType = (str.Trim().ParseOperationTypes())
                    };

                    var doRes = Arithmetic.Calc(o);

                    var msg = $"Result: {o.FirstNumber} {o.OperationType.GetString()} {o.SecondNumber} = {doRes}";

                    if (_userCommunicator.WaitReaction(msg, ref exit))
                        break;

                    _userCommunicator.Clear("insert first digit");
                    action = Action.GetFirstNumber;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return action;
        }

        private Action ParseNumber(Action action, string str, ref nint number)
        {
            try
            {
                var q = Arithmetic.TryParseNumber(str, ref action, out number);
                _userCommunicator.AskQuestion(q);
            }
            catch
            {
                _userCommunicator.ShowError("Enter number!");
                throw;
            }

            return action;
        }
    }
}