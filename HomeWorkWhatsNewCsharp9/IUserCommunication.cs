﻿namespace HomeWorkWhatsNewCsharp9
{
    public interface IUserCommunication
    {
        void ShowError(string msg);
        void AskQuestion(string msg);
        bool WaitReaction(string msg, ref bool exit);
        void Clear(string msg = "");
        bool WaitKey(ref string str, out bool @continue);
    }
}