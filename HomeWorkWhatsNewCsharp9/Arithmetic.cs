﻿using System;
using System.Linq;

namespace HomeWorkWhatsNewCsharp9
{
    public static class Arithmetic
    {
        public static string TryParseNumber(string s,ref Action i, out nint number)
        {
            CheckForNumber(s);
            i = nint.TryParse(s, out number) ? i : Action.SelectAction;
            var str = GetNextAction(ref i);
            return str;
        }

        public static void CheckForNumber(string s)
        {
            if (s.Trim().All(c => Extensions.IsDigit(c)))
                return;
            throw new InvalidOperationException("");
        }

        public static string Calc(ArithmeticOperation o)
        {
            var res = o.OperationType switch
            {
                OperationTypes.Multiplication => $"{o.FirstNumber * o.SecondNumber}",
                OperationTypes.Division => $"{o.FirstNumber / o.SecondNumber}",
                OperationTypes.Sum => $"{o.FirstNumber + o.SecondNumber}",
                OperationTypes.Difference => $"{o.FirstNumber - o.SecondNumber}",
                _ => throw new ArgumentOutOfRangeException()
            };

            return res;
        }

        public static string GetNextAction(ref Action action)
        {
            var res = "";
            switch (action)
            {
                case Action.GetFirstNumber:
                    res = "insert second digit";
                    action = Action.GetSecondNumber;
                    break;
                case Action.GetSecondNumber:
                    var s = ((OperationTypes[])Enum.GetValues(typeof(OperationTypes))).Aggregate("",
                        (current, type) => current + $"{Environment.NewLine}{type.GetString()}");
                    res = $"select operation {s}";
                    action = Action.SelectAction;
                    break;
                case Action.SelectAction:
                    break;
                default:
                    res = $"insert first digit";
                    action = Action.GetFirstNumber;
                    break;
            }
            return res;
        }
    }
}