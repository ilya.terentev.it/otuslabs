﻿using System;

namespace HomeWorkWhatsNewCsharp9
{
    public static class Extensions
    {
        //NEW FEATURE C#9 "Pattern matching enhancements"
        public static bool IsDigit(this char c) =>
            c is >= '0' and <= '9';

        public static string GetString(this OperationTypes o)
        {
            //NEW FEATURE C#8 "Switch expressions"
            return o switch
            {
                OperationTypes.Multiplication => "*",
                OperationTypes.Division => "/",
                OperationTypes.Sum => "+",
                OperationTypes.Difference => "-",
                _ => throw new ArgumentOutOfRangeException(nameof(o), o, null)
            };
        }
        public static OperationTypes ParseOperationTypes(this string o)
        {
            return o switch
            {
                "*" => OperationTypes.Multiplication ,
                "/" => OperationTypes.Division ,
                "+" => OperationTypes.Sum,
                "-" => OperationTypes.Difference,
                _ => throw new ArgumentOutOfRangeException(nameof(o), o, null)
            };
        }

    }
}