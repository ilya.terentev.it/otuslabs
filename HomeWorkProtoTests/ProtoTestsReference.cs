using HomeWorkProtoLib.Models;
using Xunit;
using static Xunit.Assert;

namespace HomeWorkProtoTests
{
    public class ProtoTestsReference
    {
        [Fact]
        public void ManTestReference()
        {
            var man = new Man("Name");
            var copy = man;
            Equal(man, copy);
            man.Name += "Copy";
            Equal(man, copy);

            var equals = ReferenceEquals(man, copy);
            Equal(true, equals);

        }
        [Fact]
        public void ParentTestReference()
        {
            var parent = new Parent("Name","Child");
            var copy = parent;
            Equal(parent, copy);
            parent.Name += "Copy";
            Equal(parent, copy);
            parent.ChildName += "Copy";
            Equal(parent, copy);

            var equals = ReferenceEquals(parent, copy);
            Equal(true, equals);
        }
        [Fact]
        public void GrandParentTestReference()
        {
            var grandParent = new GrandParent("Name", "Child","Grandchild");
            var copy = grandParent;
            Equal(grandParent, copy);
            grandParent.Name += "Copy";
            Equal(grandParent, copy);
            grandParent.ChildName += "Copy";
            Equal(grandParent, copy);
            grandParent.GrandChildName += "Copy";
            Equal(grandParent, copy);

            var equals = ReferenceEquals(grandParent, copy);
            Equal(true, equals);
        }
    }
}