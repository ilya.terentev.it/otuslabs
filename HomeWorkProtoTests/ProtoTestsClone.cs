using HomeWorkProtoLib.Models;
using Xunit;
using static Xunit.Assert;


namespace HomeWorkProtoTests
{
    public class ProtoTestsClone
    {
        [Fact]
        public void ManTestClone()
        {
            var man = new Man("Name");
            var copy = (Man)man.Clone();
            NotEqual(man, copy);
            Equal(man.Name, copy.Name);
            copy.Name += "Copy";
            NotEqual(man.Name, copy.Name);

            var equals = ReferenceEquals(man, copy);
            NotEqual(true, equals);
        }
        [Fact]
        public void ParentTestClone()
        {
            var parent = new Parent("Name", "Child");
            var copy = (Parent)parent.Clone();
            NotEqual(parent, copy);
            Equal(parent.Name, copy.Name);
            Equal(parent.ChildName, copy.ChildName);
            copy.Name += "Copy";
            NotEqual(parent.Name, copy.Name);

            var equals = ReferenceEquals(parent, copy);
            NotEqual(true, equals);
        }
        [Fact]
        public void GrandParentTestClone()
        {
            var grandParent = new GrandParent("Name", "Child", "grandson");
            var copy = (GrandParent)grandParent.Clone();
            NotEqual(grandParent, copy);
            Equal(grandParent.Name, copy.Name);
            Equal(grandParent.ChildName, copy.ChildName);
            Equal(grandParent.GrandChildName, copy.GrandChildName);
            copy.Name += "Copy";
            NotEqual(grandParent.Name, copy.Name);

            var equals = ReferenceEquals(grandParent, copy);
            NotEqual(true, equals);
        }
    }
}