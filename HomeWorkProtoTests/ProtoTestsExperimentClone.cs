using HomeWorkProtoLib.Experiment;
using Xunit;

namespace HomeWorkProtoTests
{
    public class ProtoTestsExperimentClone
    {
        [Fact]
        public void ExperimentManTestClone()
        {
            var man = new ExperimentMan("Name");
            var copy = man.Clone();
            Assert.NotEqual(man, copy);
            Assert.Equal(man.Name, copy.Name);
            copy.Name += "Copy";
            Assert.NotEqual(man.Name, copy.Name);

            var equals = ReferenceEquals(man, copy);
            Assert.NotEqual(true, equals);
        }
   
    }
}