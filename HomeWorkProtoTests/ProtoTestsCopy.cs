using HomeWorkProtoLib.Models;
using Xunit;
using static Xunit.Assert;

namespace HomeWorkProtoTests
{
    public class ProtoTestsCopy
    {
        [Fact]
        public void ManTestCopy()
        {
            var man = new Man("Name");
            var copy = man.Copy();
            NotEqual(man, copy);
            Equal(man.Name, copy.Name);
            copy.Name += "Copy";
            NotEqual(man.Name, copy.Name);

            var equals = ReferenceEquals(man, copy);
            NotEqual(true, equals);
        }
        [Fact]
        public void ParentTestCopy()
        {
            var parent = new Parent("Name", "Child");
            var copy = parent.Copy();
            NotEqual(parent, copy);
            Equal(parent.Name, copy.Name);
            Equal(parent.ChildName, copy.ChildName);
            copy.Name += "Copy";
            NotEqual(parent.Name, copy.Name);

            var equals = ReferenceEquals(parent, copy);
            NotEqual(true, equals);
        }
        [Fact]
        public void GrandParentTestCopy()
        {
            var grandParent = new GrandParent("Name", "Child","grandson");
            var copy = grandParent.Copy();
            NotEqual(grandParent, copy);
            Equal(grandParent.Name, copy.Name);
            Equal(grandParent.ChildName, copy.ChildName);
            Equal(grandParent.GrandChildName, copy.GrandChildName);
            copy.Name += "Copy";
            NotEqual(grandParent.Name, copy.Name);

            var equals = ReferenceEquals(grandParent, copy);
            NotEqual(true, equals);
        }
    }
}