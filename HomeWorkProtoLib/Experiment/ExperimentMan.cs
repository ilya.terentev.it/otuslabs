﻿using System;
using HomeWorkProtoLib.Interfaces;

namespace HomeWorkProtoLib.Experiment
{
    public class ExperimentMan : IExperimentCloneable<ExperimentMan>
    {
        public string Name;
        /// <summary>
        /// Experimental class 
        /// </summary>
        /// <param name="name">Name of the man</param>
        public ExperimentMan(string name)
        {
            Name = name;
        }
        public ExperimentMan Clone()
        {
            return new ExperimentMan(Name);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}