﻿namespace HomeWorkProtoLib.Interfaces
{

    /// <summary>
    /// Interface for implementing the prototype pattern 
    /// </summary>
    public interface IMyCloneable<out T>
    {
        public T Copy();
    }
}
