﻿using System;

namespace HomeWorkProtoLib.Interfaces
{
    /// <summary>
    /// Experimental interface inheriting from ICloneable and overriding method Clone 
    /// </summary>
    public interface IExperimentCloneable<out T>:ICloneable
    {
        public new T Clone();
    }
}