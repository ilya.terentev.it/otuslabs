# Доамашнее задание по порождающим паттернам на примере "прототипа"
Доамашнее задание в рамках курса [С# разработчик](https://otus.ru/lessons/csharp-professional/).
## Описание интерфейсов
* IMyCloneable - Интерфейс для реализации шаблона прототипа
* IExperimentCloneable - экспериментальынй интерфейс, наследующиу ICloneable и переопределяющий метод Clone
## Описание классов
* Man - базовый класс, реализующий интерфейсы ICloneable,IMyCloneable<T>
* Parent - класс расширяющий класс Man, добавлено поле ChildName
* GrandParent - класс расширяющий класс Parent, добавлено поле GrandChildName
* ExperimentMan - класс, реализующий интерфейсы IExperimentCloneable<T>