﻿using System;
using HomeWorkProtoLib.Interfaces;

namespace HomeWorkProtoLib.Models
{
    public class Man : IMyCloneable<Man>, ICloneable
    {
        public string Name;
        /// <summary>
        /// Base class
        /// </summary>
        /// <param name="name"></param>
        public Man(string name)
        {
            Name = name;
        }

        public virtual Man Copy()
        {
            return new Man(Name);
        }

        public virtual object Clone()
        {
            return Copy();
        }
    }
}