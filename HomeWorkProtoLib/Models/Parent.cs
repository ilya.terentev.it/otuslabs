﻿using System;

namespace HomeWorkProtoLib.Models
{
    public class Parent : Man, ICloneable
    {
        public string ChildName;

        public override Parent Copy()
        {
            return new Parent(Name, ChildName);
        }

        public override object Clone()
        {
            return Copy();
        }
        /// <summary>
        /// Paren class - definitely has a child, inheriting from Man
        /// </summary>
        /// <param name="name"></param>
        /// <param name="childName"></param>
        public Parent(string name, string childName) : base(name)
        {
            ChildName = childName;
        }
    }
}