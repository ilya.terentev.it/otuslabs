﻿using System;

namespace HomeWorkProtoLib.Models
{
    public class GrandParent : Parent, ICloneable
    {
        public string GrandChildName;

        public override GrandParent Copy()
        {
            return new GrandParent(Name, ChildName, GrandChildName);
        }

        public override object Clone()
        {
            return Copy();
        }
        /// <summary>
        /// GrandParent class,  definitely has a child and a grandchild (inheriting from parent)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="childName"></param>
        /// <param name="grandChildName"></param>
        public GrandParent(string name, string childName, string grandChildName) : base(name, childName)
        {
            GrandChildName = grandChildName;
        }
    }
}