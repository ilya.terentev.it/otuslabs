﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomwWorkReflection;

namespace HWReflectionTests
{
    public static class ClassExtentions
    {
        public static string StringForTest(this DefaultClass f)
        {
            return $"{f.i1};{f.i2};{f.i3};{f.i4};{f.i5}";
        }

        public static string StringForTest(this ClassWithNesting c)
        {
            var res = $"{c.i1};{c.i2};{c.i3};{c.i4};{c.i5};{c.pubString1};{c.pubString2};{c.pubString3};{c.decimal1}";
            res += $";{c.b?.d1}";
            if (c.b != null)
            {
                foreach (var d in c.b?.LitOfD)
                    res += $";{d.Name}";
                foreach (var l in c.b?.LitOfListString)
                foreach (var ll in l)
                    res += $";{ll}";

                if (c.b.c != null)
                {
                    res += $";{c.b?.c?.i33};{c.b?.c?.Enum0};{c.b.c.Enum1}";

                    foreach (var d in c.b?.c?.DoubleList)
                        res += $";{d}";

                    foreach (var d in c.b?.c?.DoubleList2)
                        res += $";{d}"; 
                }
            }

            return res;
        }
    }
}
