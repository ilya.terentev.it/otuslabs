using System.Collections.Generic;
using HomeWorkReflection;
using HomwWorkReflection;
using Xunit;

namespace HWReflectionTests
{
    public class MySerializerTests
    {
        public static IEnumerable<object[]> DefaultClassCases
            => new[]
            {
                new object[] {new DefaultClass()},
                new object[] {new DefaultClass().Get()}
            };

        public static IEnumerable<object[]> ClassWithNestingCases
            => new[]
            {
                new object[] {ClassGenerator.GetNewInstance},
                new object[] {new ClassWithNesting()}
            };

        public static IEnumerable<object[]> EnumerableCases
            => new[]
            {
                new object[] {new List<string>(){ "s1", "s2" } },
                new object[] {new List<decimal>(){ 1, 2 } },

            };
        [Theory]
        [MemberData(nameof(EnumerableCases))]
        public void SerializeEnumerable<T>(T data)
        {
            var s = new MySerializer(new Settings(";", "|"));
            var serialized = s.Serialize(data);
            var desirialized = s.Deserialize<T>(serialized);
            Assert.Equal(data, desirialized);
        }

        [Theory]
        [MemberData(nameof(DefaultClassCases))]
        public void SerializeDefaultClass<T>(T data)
        {
            var s = new MySerializer(new Settings(";", "|"));
            var serialized = s.Serialize(data);
            var desirialized = s.Deserialize<T>(serialized);
            Assert.Equal((data as DefaultClass).StringForTest(), (desirialized as DefaultClass).StringForTest());
        }


        [Theory]
        [MemberData(nameof(ClassWithNestingCases))]
        public void SerializeClassWithNesting<T>(T data)
        {
            var s = new MySerializer(new Settings(";", "|"));
            var serialized = s.Serialize(data);
            var desirialized = s.Deserialize<T>(serialized);
            var act = (data as ClassWithNesting).StringForTest();
            var exp = (desirialized as ClassWithNesting).StringForTest();
            Assert.Equal(act, exp);
        }

        [Theory]
        [InlineData("str")]
        [InlineData(1)]
        [InlineData(2.2d)]
        [InlineData(3.1f)]
        [InlineData(DigitEnum.Three)]
        public void SerializePrimitive<T>(T o)
        {
            var s = new MySerializer(new Settings(";", "|"));
            var serialized = s.Serialize(o);
            var desirialized = s.Deserialize<T>(serialized);
            Assert.Equal(o, desirialized);
        }

        [Fact]
        public void ClassNotEqualIfInstChangeAfterSerialize()
        {
            var dc = new DefaultClass().Get();
            var s = new MySerializer(new Settings(";", "|"));
            var serialized = s.Serialize(dc);
            dc.i1 = 9;
            var desirialized = s.Deserialize<DefaultClass>(serialized);
            Assert.NotEqual(dc.StringForTest(), desirialized.StringForTest());
        }
    }
}